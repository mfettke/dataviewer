//=============================================================================
//
//  File: DataViewer.h
//
//  Author: Matt Fettke
//
//  Date: 01/03/2012
//
//=============================================================================

#ifndef DATAVIEWER_H
#define DATAVIEWER_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>

#include "DataSource.h"


//  Define the pixel size constant (bytes).
//
//  NOTE: Changing this value will require a re-write of the
//        DataReader::nextImage() function.
#define PIXEL_SIZE 4

//  Define timer interval (ms).
#define TIMER_INTERVAL 33


//-----------------------------------------------------------------------------
//
//  Class: DataViewer
//
//  Description:
//    This is the main GUI element, responsible for drawing the new data.
//
//----------------------------------------------------------------------------- 
class DataViewer : public QMainWindow
{
  Q_OBJECT

public:
  DataViewer(int width, int height);
  ~DataViewer();

private:
  DataViewer(const DataViewer&);
  DataViewer& operator=(const DataViewer&);

protected slots:
  void doUpdate();

protected:
  void resizeEvent(QResizeEvent *event);
  void memoryAlloc();

private:
  int m_width;
  int m_height;
  size_t m_bufferSize;
  bool m_needResizing;
  unsigned char* m_imageBuffer;
  DataSource *m_dataSource;
  QPixmap* m_pixmap;
  QGraphicsScene *m_scene;
  QGraphicsView *m_view;
  QGraphicsPixmapItem *m_pixmapItem;
};

#endif
