// DataSource.h
//
// This module provides data as an RGBa image.

#ifndef DataSource_h
#define DataSource_h

#include <cstddef>

class DataSource
{
public:
	DataSource();
	~DataSource();
	
	// poll
	//
	// Return true if data is available, false if no data is available.
	// If data is available, this function writes the data into image_buffer.
	//
	// The x_width and y_height are the image dimensions. image_buffer
	// is the buffer to hold the image, and image_buffer_size is the
	// size of the image_buffer, in bytes.
	//
	// The image has RGBa pixels, that is, 32 bits/pixel. Therefore, the
	// image buffer size must be at least x_width * y_height * pixel_size,
	// where 'pixel_size' is 4.
	bool poll(int x_width, int y_height, unsigned char* image_buffer, size_t image_buffer_size);
	
private:
	// The copy constructor and assignment operator
	// are declared private, with no implementation,
	// so any reference to either will generate an error.
	DataSource(const DataSource&);
	DataSource& operator=(const DataSource&);

	// This counter is used to determine when to return an
	// image, and when to return nothing. The counter is
	// incremented on each call, and reset each time an
	// image is produced.
	int poll_count;
	
	// This counter is used to generate a different
	// return image on each call. The counter is incremented
	// on each image.
	int image_count;
};

#endif
