// DataSource.cpp

#include "DataSource.h"

// Constructor

DataSource::DataSource()
{
	poll_count = 0;
	image_count = 0;
}

// Destructor

DataSource::~DataSource()
{
}

// poll

bool DataSource::poll(int x_width, int y_height, unsigned char* image_buffer, size_t image_buffer_size)
{
	const int poll_interval = 5;
	
	++poll_count;
	if (poll_count < poll_interval)
		return false;
		
	poll_count = 0;
	
	const int pixel_size = 4;
	
	size_t image_size = x_width * y_height * pixel_size;
	
	if (image_size > image_buffer_size)
		return false;
		
	++image_count;
	
	// Generate a synthetic image. The image has a
	// transparency of 0 (opaque).
	unsigned char* scan = (unsigned char*) image_buffer;
	for (int y = 0; y < y_height; ++y)
	{
		for (int x = 0; x < x_width; ++x)
		{
			*scan = 0xff;
			++scan;
			*scan = (unsigned char)x;
			++scan;
			*scan = (unsigned char)y;
			++scan;
			*scan = (unsigned char)image_count;
			++scan;
		}
	}
	
	return true;
}
