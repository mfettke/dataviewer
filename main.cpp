//=============================================================================
//
//  File: main.cpp
//
//  Author: Matt Fettke
//
//  Date: 27/02/2012
//
//=============================================================================

#include <QApplication>
#include <QtCore>
#include "DataViewer.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    DataViewer dataViewer(500, 500);

    dataViewer.show();

    return app.exec();
}
