HEADERS       = DataViewer.h \
                DataSource.h
SOURCES       = DataViewer.cpp \
                DataSource.cpp \
                main.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/DataViewer
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS DataViewer.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/widgets/DataViewer
INSTALLS += target sources

