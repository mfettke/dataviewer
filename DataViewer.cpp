//=============================================================================
//
//  File: DataViewer.cpp
//
//  Author: Matt Fettke
//
//  Date: 01/03/2012
//
//=============================================================================

#include <new>
#include <cstddef>

#include "DataViewer.h"


//-----------------------------------------------------------------------------
//
//  Class: DataViewer
//
//  Description:
//    This is the main GUI element, responsible for drawing the new data.
//
//-----------------------------------------------------------------------------
DataViewer::DataViewer(int width, int height) :
  m_width(width),
  m_height(height),
  m_bufferSize(0),
  m_needResizing(false),
  m_imageBuffer(NULL),
  m_dataSource(NULL),
  m_pixmap(NULL),
  m_scene(NULL),
  m_view(NULL),
  m_pixmapItem(NULL)
{
  m_scene = new QGraphicsScene(this);
  m_dataSource = new DataSource();

  m_pixmapItem = new QGraphicsPixmapItem();
  m_scene->addItem(m_pixmapItem);

  m_view = new QGraphicsView();
  m_view->setScene(m_scene);
  m_view->setResizeAnchor(QGraphicsView::AnchorViewCenter);
  m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  m_view->resize(minimumSize());

  setCentralWidget(m_view);

  // This timer is the driver for getting new data from the data source.
  // When it fires (every TIMER_INTERVAL msec) the doUpdate() function
  // is called.
  QTimer *updateTimer = new QTimer(this);
  QObject::connect(updateTimer, SIGNAL(timeout()), this, SLOT(doUpdate()));
  updateTimer->start(TIMER_INTERVAL);

  memoryAlloc();
  resize(m_width, m_height);
}

DataViewer::~DataViewer()
{
  if (m_imageBuffer != NULL)
  {
    delete [] m_imageBuffer;
  }
  if (m_pixmap != NULL)
  {
    delete m_pixmap;
  }
  delete m_view;
  delete m_pixmapItem;
  delete m_dataSource;
  delete m_scene;
}

//  Function: doUpdate()
//
//  Description:
//    Request new data from the data reader.
//
//    This function is called every time the updateTimer fires. If the
//    reader doesn't have any new data it just returns NULL.
void DataViewer::doUpdate()
{
  if (m_needResizing == true)
  {
    memoryAlloc();
  }

  bool newData = m_dataSource->poll(m_width, m_height, m_imageBuffer, m_bufferSize);
  if (newData != false)
  {
    QImage image(m_imageBuffer, m_width, m_height, m_width*PIXEL_SIZE, QImage::Format_ARGB32);
    m_pixmap->convertFromImage(image);
    m_pixmapItem->setPixmap(*m_pixmap);
  }
}

//  Function: resizeEvent()
//
//  Description:
//    Called whenever main window is resized.
//
//    This allows us to capture the new image size and signal to the
//    data reader that the data size needs to be recalculated.
void DataViewer::resizeEvent(QResizeEvent *event)
{
  QSize newSize = event->size();
  m_width = newSize.width();
  m_height = newSize.height();
  m_needResizing = true;
}

//  Function: memoryAlloc()
//
//  Description:
//    Handles all memory allocation and resizing
void DataViewer::memoryAlloc()
{
  if (m_imageBuffer != NULL)
  {
    delete [] m_imageBuffer;
    m_imageBuffer = NULL;
  }

  m_bufferSize = m_width * m_height * PIXEL_SIZE;
  m_imageBuffer = new (std::nothrow) unsigned char[m_bufferSize];

  if (m_pixmap != NULL)
  {
    delete m_pixmap;
    m_pixmap = NULL;
  }
  m_pixmap = new QPixmap(m_width, m_height);
  
  m_needResizing = false;
}

